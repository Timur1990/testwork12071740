<?php

namespace App\Http\Controllers;

use App\Dessert;
use Illuminate\Http\Request;

class DessertController extends Controller
{
    public function index()
    {
        return Dessert::all();
    }

    public function show($id)
    {
        return Dessert::find($id);
    }

    public function store(Request $request)
    {
        $dessert = Dessert::create([
            'name' => $request->input('name'),
            'calories' => $request->input('calories')
        ]);
        return $dessert;
    }

    public function update(Request $request)
    {
        $dessert = Dessert::find($request->id);
        $dessert->name = $request->name;
        $dessert->calories = $request->calories;
        $dessert->save();
        return 'Success';
    }

    public function destroy($id)
    {
        $dessert = Dessert::find($id);
        $dessert->delete();
        return 'Success';
    }
}
