<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <title>My CRUD</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
</head>
<body>

<div id="app">
    <app-component></app-component>
</div>
{{-- @extends('layouts.app') --}}
</body>
<!-- JavaScript -->
<script src="{{ asset('js/app.js') }}"></script>
</html>
