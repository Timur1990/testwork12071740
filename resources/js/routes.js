import Vue from 'vue'
import VueRouter from 'vue-router'
import ListDesserts from './components/Dessert/ListDesserts'
import NewDessert from './components/Dessert/NewDessert'
import EditDessert from './components/Dessert/EditDessert'
import Dessert from './components/Dessert/ShowDessert'

Vue.use(VueRouter);

export default new VueRouter({
    // mode: 'history',
    routes:[
        {
            path: '/',
            name:'ListDesserts',
            component: ListDesserts
        },
        {
            path: '/dessert/:id',
            props:true,
            name:'dessert',
            component: Dessert
        },
        {
            path: '/create/dessert',
            name:'newDessert',
            component: NewDessert
        },
        {
            path: '/edit/dessert/:id/',
            props:true,
            name:'editDessert',
            component: EditDessert
        }

    ]
})