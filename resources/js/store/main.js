import Vue from 'vue'
import Vuex from 'vuex'
import dessert from './dessert'

Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        dessert
    }
})