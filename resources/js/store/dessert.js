export default{
    state:{
        desserts: []
    },
    mutations:{
        createDessert(state,payload){
            state.desserts.push(payload)
        },
        editeDessert(state,payload){
            state.desserts.splice(payload.index,1,payload)
        },
        deleteDessert(state,payload){
            state.desserts.splice(payload,1)
        }
    },
    actions:{
        createDessert({commit},payload){
           axios.post('api/dessert', payload)
               .then(function (response) {
                   payload = response.data
                   commit('createDessert',payload)
           })
           .catch(function (error) {
               console.log(error);
           })
        },

        editDessert({commit},payload){
            axios({
                method: 'put',
                url: 'api/dessert/' + payload.id,
                data: payload
            })
            commit('editeDessert',payload)
        },
        deleteDessert({commit},payload){
            axios({
                method: 'delete',
                url: 'api/dessert/' + payload[0],
                data: payload[0]
            })
            commit('deleteDessert',payload[1])
        }
    },
    getters:{
        getDessertsFromDataBase (state) {
            axios.get('api/dessert')
                .then(function (response) {
                    state.desserts = response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        listDesserts(state){
            return state.desserts
        }
    }
}